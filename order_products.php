<?php session_start(); ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>order</title>
</head>
<body>
<script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<?php

echo '<div class="basket"><h2>Shopping cart: </h2>';
$total_price = 0;
if (isset($_SESSION['cart'])) {


    foreach ($_SESSION['cart'] as $iManufacturerNumber => $item) {
        if ($item['count'] == 0) {
            continue;
        }
        if ($item['name'])
            echo '<div id="' . $iManufacturerNumber . '"><div><span> Name: ' . $item['name'] . '</span>
<span> Price:' . $item['total_price'] . '</span>
<span>  Count: </span> <input data-id="' . $iManufacturerNumber . '" value=" ' . $item['count'] . '"></div>';
        echo '<div><button class="minus" type="button" name="minus" value="' . $iManufacturerNumber . '">minus</button>
<button class="plus" type="button" name="plus" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">plus</button>
<button class="delete" type="button" name="delete" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">delete</button>
</div></div>';

        $total_price += $item['total_price'];
    }

}
//echo '<p class="total-price">Total price: ' . $total_price . '</p>';
echo '</div><div class="total-price"><p>Total price: ' . $total_price . '</p></div>';

?>
<div class="form">
    <form method="post" action="order.php">
        <label>Name</label>
        <input type="text" name="name"><br/>
        <label>Last name: </label>
        <input type="text" name="last_name"><br/>
        <label>Address: </label>
        <input type="text" name="address"><br/>
        <input type="submit" value="checkout" name="checkout">
    </form>
</div>
<script>
    $(document).ready(function () {
        $(document).on('click', '.minus', function () {
            let id = $(this).val();
            console.log(id);
            $.ajax({
                type: "POST",
                url: "handler.php",
                dataType: 'json',
                data: {iManufacturerNumberMinus: id},
                success: function (data) {
                    if (data.error == undefined) {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    } else {
                        $("div#" + id).html(data.html);
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    }
                }
            });

        });
        $(document).on('click', '.plus', function () {
            let id = $(this).data('id'),
                count = parseInt(($("input[data-id=" + id + "]").val()).trim()) + 1;
            console.log(id)
            console.log(count)
            $.ajax({
                type: "POST",
                url: "handler.php",
                dataType: 'json',
                data: {iManufacturerNumberPlus: id, count: count},
                success: function (data) {
                    if (data.error == undefined) {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    } else {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                        alert("Not enough goods in stock");
                    }
                }
            });

        });

        $(document).on('change', 'input', function () {
            let id = $(this).parent().parent().attr("id");
            let count = $(this).val();
            if (count < 0) {
                alert('Enter correct value');
                $(this).val(1);
                count = 1;
            }
            $.ajax({
                type: "POST",
                url: "handler.php",
                dataType: 'json',
                data: {iManufacturerNumberInput: id, count: count},
                success: function (data) {
                    if (data.error == undefined) {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    } else {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                        alert("Not enough goods in stock");
                    }

                }
            });

        });

        $(document).on('click', '.delete', function () {
            let id = $(this).parent().parent().attr("id");
            $.ajax({
                type: "POST",
                url: "handler.php",
                dataType: 'json',
                data: {iManufacturerNumberDelete: id},
                success: function (data) {
                    if (data.delete !== undefined) {
                        $("div#" + id).html(data.html);
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    }
                }
            });

        });
    });
</script>
</body>
</html>
