<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>HTML Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
    a {
        padding: 10px;
    }

    .products {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }

    .product {
        width: 300px;
    }

    input {
        text-align: center;
    }

    img {
        width: 300px;
        height: 300px;
    }
</style>

<?php
// a.b.c.test
// [
// a =>
//    [
//    b =>
//  [
//      c = test
//      ]
//]
//]
require_once("config.php");
$dbh->query("DELETE FROM berlin_tires");
$isFirst = true;

$csv = fopen('BerlinTires.csv', "r");
$names = $insert_data = [];
$map = [];
$count = 0;

while (($data = fgetcsv($csv, 0, ";")) !== FALSE) {
    if ($isFirst) {
        $names = $data;
        $isFirst = false;
        continue;
    }
    $insert_data[] = $data;
}

$stmt = 'INSERT INTO berlin_tires (`iManufacturer`,`iManufacturerNumber`,`iEAN13`,`iTitle`,`iGroup`,`iTyp`,`iDesign`,
                          `iSeason`, `iStock`, `iPrice1`, `iPicturesmall`, `ipicturebig`, `iPallet_quantity`,`isizeTx1`,
                          `isizetx2`,`iPackWidth_mm`, `iPackHeight_mm`, `iPackLength_mm`, `iPackWeight_gr`, `rDiameter`,
                          `rBasewidth`, `rholes`, `rpcd1`, `rpcd2` , `roffset`, `rCH`, `rcolor`, `rcolorL`,`rAxleLoad`, `tRatio`,	
                        `tSI`, `tLI1`, `tLI2`, `tExtraLoad`,`tDOT`,`tEULabel`,`tRollingresistance`, `tWetGrip`, 
                          `tNoiseemissionValue`,`tNoiseemissionCount`,`tClass`) VALUES  ';
$chunk_array = array_chunk($insert_data, 500);

$filler = array_fill(0, count($names), '?');
//var_dump(count($filler));
$filler = '(' . implode(', ', $filler) . ')';

foreach ($chunk_array as $item) {
    //var_dump(count($item));
    $fill_data = array_fill(0, count($item), $filler);
    $sql = implode(', ', $fill_data) . ';';
    //var_dump($stmt, count($names), count($item[0]), count($fill_data));
    $stmt2 = $dbh->prepare($stmt . $sql);
    $result = array_merge_recursive(...$item);
    //var_dump($result);
    $stmt2->execute($result);
    //var_dump($stmt2->errorInfo());
}

function getIManufacturerNumber($iManufacturerNumber, $dbh)
{
    $sql = "SELECT iTitle, iPrice1, iManufacturerNumber FROM berlin_tires WHERE `iManufacturerNumber` = (?)";
    $stmt = $dbh->prepare($sql);
    $stmt->execute([$iManufacturerNumber]);
    return $stmt->fetch();
}

/*if (isset($_POST['buy']) || isset($_POST['minus']) || isset($_POST['plus'])) {
    //dd($_SESSION['cart']);
    $_SESSION['cart'] = isset($_SESSION['cart']) ? $_SESSION['cart'] : [];
    $iManufacturerNumber = $_POST['price'];


    if ($_POST['buy']) {
        if (isset($_SESSION['cart'][$iManufacturerNumber])) {
            $_SESSION['cart'][$iManufacturerNumber]['total_price'] += $_SESSION['cart'][$iManufacturerNumber]['price'];
            $_SESSION['cart'][$iManufacturerNumber]['count']++;

        } else {
            $data = getIManufacturerNumber($iManufacturerNumber, $dbh);
            $price = $data[1];
            $name = $data[0];
            $iManufacturerNumber = $data[2];
            $item = [
                'name' => $name,
                'price' => $price,
                'total_price' => $price,
                'count' => 1
            ];
            $_SESSION['cart'][$iManufacturerNumber] = $item;
        }
    }
    if ($_POST['plus']) {
        $iManufacturerNumber = $_POST['plus'];
        if (isset($_SESSION['cart'][$iManufacturerNumber])) {
            $_SESSION['cart'][$iManufacturerNumber]['total_price'] += $_SESSION['cart'][$iManufacturerNumber]['price'];
            $_SESSION['cart'][$iManufacturerNumber]['count']++;
        }

    } else if ($_POST['minus']) {
        $iManufacturerNumber = $_POST['minus'];
        if (isset($_SESSION['cart'][$iManufacturerNumber])) {
            $_SESSION['cart'][$iManufacturerNumber]['total_price'] -= $_SESSION['cart'][$iManufacturerNumber]['price'];
            $_SESSION['cart'][$iManufacturerNumber]['count']--;
            if ($_SESSION['cart'][$iManufacturerNumber]['count'] == 0) {
                unset($_SESSION['cart'][$iManufacturerNumber]);
            }
        }

    }
}*/
echo '<div class="order">';
$total_price = 0;
if (isset($_SESSION['cart'])) {
    echo '<form action="order_products.php"><input type="submit" value="Order products"></form>';
/*    foreach ($_SESSION['cart'] as $iManufacturerNumber => $item) {
        if ($item['count'] == 0) {
            continue;
        }
        if ($item['name'])
            echo '<div id="' . $iManufacturerNumber . '"><div><span> Name: ' . $item['name'] . '</span>
<span> Price:' . $item['total_price'] . '</span>
<span>  Count: </span> <input data-id="' . $iManufacturerNumber . '" value=" ' . $item['count'] . '"></div>';
        echo '<div><button class="minus" type="button" name="minus" value="' . $iManufacturerNumber . '">minus</button>
<button class="plus" type="button" name="plus" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">plus</button>
<button class="delete" type="button" name="delete" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">delete</button>
</div></div>';

        $total_price += $item['total_price'];
    }*/
}
//echo '<p class="total-price">Total price: ' . $total_price . '</p>';
echo '</div><h3>Products: </h3><div class="products">';


if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

$size_page = 20;
$offset = ($page - 1) * $size_page;

$total_rows = $dbh->query("SELECT COUNT(*) FROM berlin_tires")->fetchColumn();
$total_pages = ceil($total_rows / $size_page);

$res = $dbh->query("SELECT iTitle, iPrice1,iManufacturerNumber, ipicturebig FROM berlin_tires LIMIT $offset, $size_page;")->fetchAll();
/*foreach ($res as $item) {
    echo $item['iTitle'] . " Price: <span class='price'>" . $item['iPrice1'] . "</span> <form method='post'> <button type='button' name='buy'  value='buy'>buy</button>
 <input type='hidden' name='price' value='" . $item['iManufacturerNumber'] . "'>
 </form>";
}*/
foreach ($res as $item) {
    /*    echo $item['iTitle'] . " Price: <span class='price'>" . $item['iPrice1'] . "</span> <form action='handler.php' method='post'> <button class='buy' type='button' name='iManufacturerNumber'  value='" . $item['iManufacturerNumber'] . "'>buy</button>
     <input type='hidden' name='price' value='" . $item['iManufacturerNumber'] . "'>
     </form>";*/
    echo '<div class="product">' . $item['iTitle'] . ' <img src="' . $item['ipicturebig'] . '"> Price: <span class="price"> ' . $item['iPrice1'] . '</span>
<button class="buy" type="button" name="iManufacturerNumber"  value="' . $item['iManufacturerNumber'] . '">buy</button>
 <input type="hidden" name="price" value="' . $item['iManufacturerNumber'] . '"></div>';
}
echo "</div>";
?>
<ul class="pagination">
    <li><a href="?page=1">First</a></li>
    <li class="<?php if ($page <= 1) {
        echo 'disabled';
    } ?>">
        <a href="<?php if ($page <= 1) {
            echo '#';
        } else {
            echo "?page=" . ($page - 1);
        } ?>">Prev</a>
    </li>
    <li class="<?php if ($page >= $total_pages) {
        echo 'disabled';
    } ?>">
        <a href="<?php if ($page >= $total_pages) {
            echo '#';
        } else {
            echo "?page=" . ($page + 1);
        } ?>">Next</a>
    </li>
    <li><a href="?page=<?php echo $total_pages; ?>">Last</a></li>
</ul>
<script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>

    $(document).ready(function () {
        $("button.buy").click(function () {
            $("div.order").html('<form action="order_products.php"><input type="submit" value="Order products"></form>');
            let id = $(this).val();
            let count = $("input[data-id=" + id + "]");//.val().trim() + 1;
            if(count.val() !== undefined) {
                count = +(count.val()).trim() + 1;
                console.log(count)
            } else {
                count = 1;
            }
            $.ajax({
                type: "POST",
                url: "handler.php",
                dataType: 'json',
                data: {iManufacturerNumber: id, count: count},
                success: function (data) {
                    if (data.error == undefined) {
                        if (data.html !== undefined) {
                            //$("div#" + id).html(data.html);
                            //$("div.total-price").html(data.total_price);
                        } else if (data.append !== undefined) {
                            //$("div.basket").append(data.append);
                            //$("div.total-price").html(data.total_price);
                        }
                    } else {
                        alert("Not enough goods in stock");
                        //$("div.total-price").html(data.total_price);
                        //$("div#" + id).html(data.html);
                    }
                }
            });

        });

        $(document).on('click', '.minus', function () {
            let id = $(this).val();
            console.log(id)
            $.ajax({
                type: "POST",
                url: "handler.php",
                dataType: 'json',
                data: {iManufacturerNumberMinus: id},
                success: function (data) {
                    if (data.error == undefined) {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    } else {
                        $("div#" + id).html(data.html);
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    }
                }
            });

        });
        $(document).on('click', '.plus', function () {
            let id = $(this).data('id'),
                count = parseInt(($("input[data-id=" + id + "]").val()).trim()) + 1;
            console.log(id)
            console.log(count)
            $.ajax({
                type: "POST",
                url: "handler.php",
                dataType: 'json',
                data: {iManufacturerNumberPlus: id, count: count},
                success: function (data) {
                    if (data.error == undefined) {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    } else {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                        alert("Not enough goods in stock");
                    }
                }
            });

        });

        $(document).on('change', 'input', function () {
            let id = $(this).parent().parent().attr("id");
            let count = $(this).val();
            if (count < 0) {
                alert('Enter correct value');
                $(this).val(1);
                count = 1;
            }
            $.ajax({
                type: "POST",
                url: "handler.php",
                dataType: 'json',
                data: {iManufacturerNumberInput: id, count: count},
                success: function (data) {
                    if (data.error == undefined) {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    } else {
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                        alert("Not enough goods in stock");
                    }

                }
            });

        });

        $(document).on('click', '.delete', function () {
            let id = $(this).parent().parent().attr("id");
            $.ajax({
                type: "POST",
                url: "handler.php",
                dataType: 'json',
                data: {iManufacturerNumberDelete: id},
                success: function (data) {
                    if (data.delete !== undefined) {
                        $("div#" + id).html(data.html);
                        $("div#" + id + '>div:first-child').html(data.html);
                        $("div.total-price").html(data.total_price);
                    }
                }
            });

        });
    });
</script>
</body>
</html>
