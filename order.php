<?php
session_start();
require_once("config.php");
$last_name = $_POST['last_name'];
$name = $_POST['name'];
$address = $_POST['address'];
if (isset($_POST['checkout'])) {
    $sql = 'INSERT INTO orders (`last_name`, `name`, `address`) VALUES (?,?,?)';
    $stmt = $dbh->prepare($sql);
    $stmt->execute(["$last_name","$name","$address"]);
    $dbh->query($sql);
    $id = $dbh->lastInsertId();
    foreach ($_SESSION['cart'] as $item) {
        $sql = 'INSERT INTO products (`id_product`, `id_order`, `count`) VALUES (?,?,?)';
        $stmt = $dbh->prepare($sql);
        $stmt->execute([$item['id_product'], $id,$item['count']]);
        $sql = 'UPDATE `berlin_tires` SET iStock = ? WHERE `iManufacturerNumber` = ?';
        $stmt = $dbh->prepare($sql);
        $newStock = $item['stock'] - $item['count'];
        $stmt->execute([$newStock, $item['id_product']]);
    }
    unset($_SESSION['cart']);
}
echo '<a href="index.php">Home page</a>';
