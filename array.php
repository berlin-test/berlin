<?php
$items = [
    ['id' => 2, 'name' => 'A-1', 'parent_id' => 1],
    ['id' => 1, 'name' => 'A', 'parent_id' => null],
    ['id' => 3, 'name' => 'A-1-1', 'parent_id' => 2],
    ['id' => 5, 'name' => 'A-1-1-2', 'parent_id' => 3],
    ['id' => 6, 'name' => 'A-2', 'parent_id' => 1],
    ['id' => 7, 'name' => 'A-2-2-1', 'parent_id' => 6],
    ['id' => 4, 'name' => 'A-1-1-1', 'parent_id' => 3],
    ['id' => 8, 'name' => 'A-2-2-2', 'parent_id' => 6],
    ['id' => 9, 'name' => 'A-2-2-1-1', 'parent_id' => 7],
    ['id' => 10, 'name' => 'B', 'parent_id' => null],
];

function buildTree($items, $parent = null)
{
    $category = [];
    foreach ($items as $item) {
        if ($item['parent_id'] == $parent) {
            $children = buildTree($items, $item['id']);
            if ($children) {
                $item['children'] = $children;
            }
            $category[] = $item;
        }
    }
    return $category;
}

$category = buildTree($items);
var_dump($category);
