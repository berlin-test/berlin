<?php
session_start();
function getIManufacturerNumber($iManufacturerNumber)
{
    require_once("config.php");
    $sql = "SELECT iTitle, iPrice1, iStock FROM berlin_tires WHERE `iManufacturerNumber` = (?)";
    $stmt = $dbh->prepare($sql);
    $stmt->execute([$iManufacturerNumber]);
    return $stmt->fetch();
}

function total_price($cart)
{
    $total_price = 0;
    foreach ($cart as $item) {
        $total_price += $item['total_price'];
    }
    return $total_price;
}

if (isset($_POST['iManufacturerNumber'])) {

    $iManufacturerNumber = $_POST['iManufacturerNumber'];
    if (isset($_SESSION['cart'][$iManufacturerNumber])) {
        if ($_POST['count'] > $_SESSION['cart'][$iManufacturerNumber]['stock']) {
            $_SESSION['cart'][$iManufacturerNumber]['total_price'] = $_SESSION['cart'][$iManufacturerNumber]['stock'] * $_SESSION['cart'][$iManufacturerNumber]['price'];
            $_SESSION['cart'][$iManufacturerNumber]['count'] = $_SESSION['cart'][$iManufacturerNumber]['stock'];
            $total_price = total_price($_SESSION['cart']);
            $total_price = '<p>Total price: ' . $total_price . '</p>';
            $html = '<div><span>Name: ' . $_SESSION['cart'][$iManufacturerNumber]['name'] . '</span><span>Price: ' . $_SESSION['cart'][$iManufacturerNumber]['total_price'] . '</span><span>Count: </span>
<input data-id="' . $iManufacturerNumber . '" value="' . $_SESSION['cart'][$iManufacturerNumber]['count'] . '">
</div>
<div><button class="minus" type="button" name="minus" value="' . $iManufacturerNumber . '">minus</button>
<button  class="plus" type="button" name="plus" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">plus</button>
<button class="delete" type="button" name="delete" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">delete</button>
</div>
</div>';
            print_r(json_encode(["html" => $html, 'total_price' => $total_price, 'error' => 'error']));
        } else {
            $_SESSION['cart'][$iManufacturerNumber]['total_price'] += $_SESSION['cart'][$iManufacturerNumber]['price'];
            $_SESSION['cart'][$iManufacturerNumber]['count']++;
            $total_price = total_price($_SESSION['cart']);
            $total_price = '<p>Total price: ' . $total_price . '</p>';
            $html = '<div><span>Name: ' . $_SESSION['cart'][$iManufacturerNumber]['name'] . '</span><span>Price: ' . $_SESSION['cart'][$iManufacturerNumber]['total_price'] . '</span><span>Count: </span>
<input data-id="' . $iManufacturerNumber . '" value="' . $_SESSION['cart'][$iManufacturerNumber]['count'] . '">
</div>
<div><button class="minus" type="button" name="minus" value="' . $iManufacturerNumber . '">minus</button>
<button  class="plus" type="button" name="plus" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">plus</button>
<button class="delete" type="button" name="delete" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">delete</button>
</div>
</div>';
            print_r(json_encode(["html" => $html, 'total_price' => $total_price]));
        }
    } else {
        $data = getIManufacturerNumber($iManufacturerNumber);
        $name = $data[0];
        $price = $data[1];
        $stock = $data[2];


        $item = [
            'name' => $name,
            'price' => $price,
            'total_price' => $price,
            'stock' => $stock,
            'count' => 1,
            'id_product' => $iManufacturerNumber
        ];
        $_SESSION['cart'][$iManufacturerNumber] = $item;
        $html = '<div id="' . $iManufacturerNumber . '">
<div><span>Name: ' . $name . '</span><span>Price: ' . $price . '</span><span>Count: </span>
<input data-id="' . $iManufacturerNumber . '" value="' . $_SESSION['cart'][$iManufacturerNumber]['count'] . '">
</div>
<div><button class="minus" type="button" name="minus" value="' . $iManufacturerNumber . '">minus</button>
<button  class="plus" type="button" name="plus" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">plus</button>
<button class="delete" type="button" name="delete" data-id="' . $iManufacturerNumber . '" value="' . $iManufacturerNumber . '">delete</button>
</div>
</div>';
        $total_price = total_price($_SESSION['cart']);
        $total_price = '<p>Total price: ' . $total_price . '</p>';
        print_r(json_encode(['append' => $html, 'total_price' => $total_price]));
    }

} else if (isset($_POST['iManufacturerNumberMinus'])) {
    $iManufacturerNumber = $_POST['iManufacturerNumberMinus'];
    $total_price = total_price($_SESSION['cart']);
    $total_price = '<p>Total price:' . $total_price . '</p>';
    if (isset($_SESSION['cart'][$iManufacturerNumber])) {
        $_SESSION['cart'][$iManufacturerNumber]['total_price'] -= $_SESSION['cart'][$iManufacturerNumber]['price'];
        $_SESSION['cart'][$iManufacturerNumber]['count']--;
        if ($_SESSION['cart'][$iManufacturerNumber]['count'] == 0) {
            unset($_SESSION['cart'][$iManufacturerNumber]);
            $html = '';
            $total_price = total_price($_SESSION['cart']);
            $total_price = '<p>Total price:' . $total_price . '</p>';
            print_r(json_encode(['html' => $html, 'total_price' => $total_price, 'error' => 'error']));
        } else {
            $html = '<span> Name: ' . $_SESSION['cart'][$iManufacturerNumber]['name'] . '</span>
<span>Price: ' . $_SESSION['cart'][$iManufacturerNumber]['total_price'] . '</span>
<span>Count:</span>
<input data-id="' . $iManufacturerNumber . '" value="' . $_SESSION['cart'][$iManufacturerNumber]['count'] . '">';
            print_r(json_encode(['html' => $html, 'total_price' => $total_price]));
        }
    }

} else if ($_POST['iManufacturerNumberPlus'] && $_POST['count']) {
    $iManufacturerNumber = $_POST['iManufacturerNumberPlus'];
    $count = $_POST['count'];
    if (isset($_SESSION['cart'][$iManufacturerNumber])) {
        if ($count > $_SESSION['cart'][$iManufacturerNumber]['stock']) {
            $_SESSION['cart'][$iManufacturerNumber]['total_price'] = $_SESSION['cart'][$iManufacturerNumber]['stock'] * $_SESSION['cart'][$iManufacturerNumber]['price'];
            $_SESSION['cart'][$iManufacturerNumber]['count'] = $_SESSION['cart'][$iManufacturerNumber]['stock'];
            $html = '<span> Name: ' . $_SESSION['cart'][$iManufacturerNumber]['name'] . '</span>
<span>Price: ' . $_SESSION['cart'][$iManufacturerNumber]['total_price'] . '</span>
<span>Count:</span>
<input data-id="' . $iManufacturerNumber . '" value="' . $_SESSION['cart'][$iManufacturerNumber]['count'] . '">';
            $total_price = total_price($_SESSION['cart']);
            $total_price = '<p>Total price: ' . $total_price . '</p>';
            print_r(json_encode(['html' => $html, "total_price" => $total_price, 'error' => 'error']));
        } else {
            $_SESSION['cart'][$iManufacturerNumber]['total_price'] += $_SESSION['cart'][$iManufacturerNumber]['price'];
            $_SESSION['cart'][$iManufacturerNumber]['count']++;
            $total_price = total_price($_SESSION['cart']);
            $total_price = '<p>Total price: ' . $total_price . '</p>';
            $html = '<span> Name: ' . $_SESSION['cart'][$iManufacturerNumber]['name'] . '</span>
<span>Price: ' . $_SESSION['cart'][$iManufacturerNumber]['total_price'] . '</span>
<span>Count:</span>
<input data-id="' . $iManufacturerNumber . '" value="' . $_SESSION['cart'][$iManufacturerNumber]['count'] . '">';
            print_r(json_encode($data = ['html' => $html, "total_price" => $total_price]));
        }
    }
} else if ($_POST['iManufacturerNumberInput'] && $_POST['count']) {
    $iManufacturerNumber = $_POST['iManufacturerNumberInput'];
    $count = $_POST['count'];
    if (isset($_SESSION['cart'][$iManufacturerNumber])) {
        if ($count > $_SESSION['cart'][$iManufacturerNumber]['stock']) {
            $_SESSION['cart'][$iManufacturerNumber]['total_price'] = $_SESSION['cart'][$iManufacturerNumber]['stock'] * $_SESSION['cart'][$iManufacturerNumber]['price'];
            $_SESSION['cart'][$iManufacturerNumber]['count'] = $_SESSION['cart'][$iManufacturerNumber]['stock'];
            $total_price = total_price($_SESSION['cart']);
            $total_price = '<p>Total price: ' . $total_price . '</p>';
            $html = '<span> Name: ' . $_SESSION['cart'][$iManufacturerNumber]['name'] . '</span>
<span>Price: ' . $_SESSION['cart'][$iManufacturerNumber]['total_price'] . '</span>
<span>Count:</span>
<input data-id="' . $iManufacturerNumber . '" value="' . $_SESSION['cart'][$iManufacturerNumber]['count'] . '">';
            print_r(json_encode($data = ['html' => $html, "total_price" => $total_price, 'error' => 'error']));
        } else {
            $_SESSION['cart'][$iManufacturerNumber]['total_price'] = $_SESSION['cart'][$iManufacturerNumber]['price'] * $count;
            $_SESSION['cart'][$iManufacturerNumber]['count'] = $count;
            $total_price = total_price($_SESSION['cart']);
            $total_price = '<p>Total price: ' . $total_price . '</p>';
            $html = '<span> Name: ' . $_SESSION['cart'][$iManufacturerNumber]['name'] . '</span>
<span>Price: ' . $_SESSION['cart'][$iManufacturerNumber]['total_price'] . '</span>
<span>Count:</span>
<input data-id="' . $iManufacturerNumber . '" value="' . $_SESSION['cart'][$iManufacturerNumber]['count'] . '">';
            print_r(json_encode($data = ['html' => $html, "total_price" => $total_price]));
        }
    }
} else if ($_POST['iManufacturerNumberDelete']) {
    $iManufacturerNumber = $_POST['iManufacturerNumberDelete'];
    if (isset($_SESSION['cart'][$iManufacturerNumber])) {
        unset($_SESSION['cart'][$iManufacturerNumber]);
        $html = '';
        $total_price = total_price($_SESSION['cart']);
        $total_price = '<p>Total price:' . $total_price . '</p>';
        print_r(json_encode(['html' => $html, 'total_price' => $total_price, 'delete' => 'error']));
    }
}

